/*
 * TenCountRecursive.groovy
 */

helper(10);
    
void helper(int n) {
    if (n == 1) {
        print(n+",")
    } else {
        helper(n-1)
        print(n)
        if (n < 10) {
            print(",")
        } else {
            println("")
        }
    }
}
