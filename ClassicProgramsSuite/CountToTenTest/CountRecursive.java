/*
 * CountRecursive.java
 */

public class CountRecursive {
    public static void main(String[] argv) {
        helper(10);
    }
    
    private static void helper(final int n) {
        if (n == 1) {
            System.out.print(1+",");
        } else {
            helper(n-1);
            System.out.print(n);
            if (n < 10) {
                System.out.print(",");
            } else {
                System.out.println("");
            }
        }
    }
}