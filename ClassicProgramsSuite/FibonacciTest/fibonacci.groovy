//
// fibonacci.groovy
//

def j = 1
def k = 1
print "1,1"
for (i in 3..20) {
    def next = j+k
    j = k
    k = next
    print ",${next}"
}
println ""