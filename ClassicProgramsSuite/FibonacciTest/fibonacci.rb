#
# fibonacci.ruby
#

# Zadok is free software licenced under the GNU General Public Licence (GPL),
# version 2 or any later version. Read COPYING.txt for the full licence.

def fib_up_to(max)
    f1, f2 = 1, 2
    while f1 <= max
        print(",", f1)
        f1, f2 = f2, f1+f2
    end
end

print(1)
fib_up_to(6765)
print("\n")
