/*
 * fibonacci.c
 *
 * Generate the first twenty fibonacci numbers by recursion.
 * (c) Simon P. Chappell, 14 March 2005
 *
 * Zadok is free software licenced under the GNU General Public Licence (GPL),
 * version 2 or any later version. Read COPYING.txt for the full licence.
 *
 */

#include <stdio.h>
main() {
    int i;
    printf("1");
    for (i = 2; i<=20; i++) {
        printf(",%d", fibonacci(i));
    }
    printf("\n");
}

int fibonacci(int n) {
    if (n <= 1)
        return n;
    else
        return (fibonacci(n-1) + fibonacci(n-2));
}
