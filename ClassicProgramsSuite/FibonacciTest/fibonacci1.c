/*
 * fibonacci.c
 *
 * Generate the first twenty fibonacci numbers by iteration.
 * (c) Simon P. Chappell, 14 March 2005
 *
 * Zadok is free software licenced under the GNU General Public Licence (GPL),
 * version 2 or any later version. Read COPYING.txt for the full licence.
 *
 */

#include <stdio.h>
main() {
    int i;
    int j = 1;
    int k = 1;
    int next;
    printf("1,1");
    for (i = 2; i<20; i++) {
        next = j + k;
        j = k;
        k = next;
        printf(",%d", next);
    }
    printf("\n");
} 

