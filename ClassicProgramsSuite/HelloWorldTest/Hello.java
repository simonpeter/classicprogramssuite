/*
 * Hello.java
 */

// Zadok - A multi-language test suite runner.
// Copyright (C) 2004  Simon P. Chappell
//
// Zadok is free software licenced under the GNU General Public Licence (GPL),
// version 2 or any later version. Read COPYING.txt for the full licence.

/**
 * @author  Simon P. Chappell
 * @version 3 December 2004
 */
public class Hello {
    public static void main(String[] argv) {
        System.out.println("hello, world");
    }
}
