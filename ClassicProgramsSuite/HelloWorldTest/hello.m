/*
 * hello.m
 *
 * (c) Simon P. Chappell, 14 March 2005
 */

// Zadok - A multi-language test suite runner.
// Copyright (C) 2004  Simon P. Chappell
//
// Zadok is free software licenced under the GNU General Public Licence (GPL),
// version 2 or any later version. Read COPYING.txt for the full licence.

#include <stdio.h>
main() {
    printf("hello, world\n");
}
