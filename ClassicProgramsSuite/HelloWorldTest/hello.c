/*
 * hello.c
 */

// Zadok - A multi-language test suite runner.
// Copyright (C) 2004  Simon P. Chappell
//
// Zadok is free software licenced under the GNU General Public Licence (GPL),
// version 2 or any later version. Read COPYING.txt for the full licence.

#include <stdio.h>
main() {
    printf("hello, world\n");
}
