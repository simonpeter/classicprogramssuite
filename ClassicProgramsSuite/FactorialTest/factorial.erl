%%
%% factorial.erl
%%

%% Zadok - A multi-language test suite runner.
%% Copyright (C) 2004  Simon P. Chappell
%%
%% Zadok is free software licenced under the GNU General Public Licence (GPL),
%% version 2 or any later version. Read COPYING.txt for the full licence.

-module(factorial).
-export([start/0]).

start() ->
        factorial(10,10).

factorial(0,Orig) -> (Orig-Orig);
factorial(1,Orig) ->
                    io:format("~.b,", [1]),
                    (Orig+1-Orig);
factorial(N,Orig) ->
                    F = factorial(N-1, Orig),
                    V = (N * F),
                    io:format("~.b", [V]),
                    if 
                        N == Orig -> io:format("\n");
                        N /= Orig -> io:format(",")
                    end,
                    V.
