#
# factorial.rb
#

# Zadok - A multi-language test suite runner.
# Copyright (C) 2004  Simon P. Chappell
#
# Zadok is free software licenced under the GNU General Public Licence (GPL),
# version 2 or any later version. Read COPYING.txt for the full licence.

def factorial(n)
    if n==1
        n
    else
        n * factorial(n-1)
    end
end

print(factorial(1))
2.upto(10) do |x|
    print ",", factorial(x)
end