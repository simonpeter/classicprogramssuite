//
// f2c.groovy
//

import java.text.DecimalFormat

lower = 0
step = 20
upper = 100
fmt = new DecimalFormat("##0.#")

fahr = lower
while (fahr <= upper) {
    celsius = (5.0/9.0) * (fahr-32.0)
    println(fahr + "\t" + fmt.format(celsius))
    fahr = fahr + step
}