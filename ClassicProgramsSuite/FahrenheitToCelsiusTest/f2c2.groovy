//
// f2c2.groovy
//

import java.text.DecimalFormat

fmt = new DecimalFormat("##0.#")

temps = [0, 20, 40, 60, 80, 100]
for (fahr in temps) {
    celsius = (5.0/9.0) * (fahr-32.0)
    println(fahr + "\t" + fmt.format(celsius))
}