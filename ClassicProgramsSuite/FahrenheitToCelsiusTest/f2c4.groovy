//
// f2c4.groovy
//

import java.text.DecimalFormat

final LOWER = 0
final UPPER = 100
final STEP  = 20

fmt = new DecimalFormat("##0.#")

for (fahr in (LOWER .. UPPER).step(STEP)) {
    celsius = (5.0/9.0) * (fahr-32.0)
    println(fahr + "\t" + fmt.format(celsius))
}