//
// f2c5.groovy
//

import java.text.DecimalFormat

def fmt = new DecimalFormat("##0.#")

(0..100).step(20) { fahr ->
    celsius = (5/9) * (fahr-32)
    println(fahr + "\t" + fmt.format(celsius))
}