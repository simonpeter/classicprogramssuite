//
// f2c3.groovy
//

import java.text.DecimalFormat

lower = 0
step = 20
upper = 100
fmt = new DecimalFormat("##0.#")


for (fahr in (lower .. upper).step(step)) {
    celsius = (5.0/9.0) * (fahr-32.0)
    println(fahr + "\t" + fmt.format(celsius))
}